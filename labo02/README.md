# Laboratoire 2: Fonctions et type

## 1 - Signature et règles de correspondance

Rendez-vous sur la page de [documentation officielle du module
`Data.List`](https://hackage.haskell.org/package/base-4.10.1.0/docs/Data-List.html)
de Haskell.

Portez attention en particulier aux signatures et aux règles de correspondance
des fonctions qui sont fournies par le module. Ne vous inquiétez pas si vous ne
comprenez pas tout ce qui y apparaît (comme les mots `Maybe`, `Foldable`,
etc.), ceux-si seront expliqués dans des cours ultérieurs.

Par contre, voici quelques explications sommaires sur les expressions `Num a
=>`, `Ord a =>` et `Eq a =>`, que nous verrons plus en détails dans les
prochains cours:

- L'expression `Num a =>` indique que le type `a` doit supporter les opérations
  habituelles auxquelles on s'attend pour un type *numérique*, par exemple `+`,
  `-`, `*`, etc.
- L'expression `Ord a =>` indique que les éléments de type `a` doivent être
  *comparables*, c'est-à-dire qu'on peut toujours évaluer les expressions `x <
  y`, `x == y` et `x > y`.
- Finalement, l'expression `Eq a =>` indique que les éléments de type `a`
  doivent être *égalisables*, c'est-à-dire qu'on peut évaluer `x == y`.

Familiarisez-vous plus précisément avec les fonctions suivantes (notez que j'ai
simplifié certaines signatures, vous pouvez donc utiliser ces fonctions comme
si leur signature était celle que j'indique):
```haskell
-- Fonctions de base
(++) :: [a] -> [a] -> [a]                -- Concaténation de deux listes
head :: [a] -> a                         -- Premier élément d'une liste
last :: [a] -> a                         -- Dernier élément d'une liste
tail :: [a] -> [a]                       -- Queue d'une liste
init :: [a] -> [a]                       -- Partie initiale d'une liste
null :: [a] -> Bool                      -- Indique si une liste est vide
length :: [a] -> Int                     -- Longueur d'une liste
replicate :: Int -> a -> [a]             -- Répète un élément plusieurs fois dans une liste
(!!) :: [a] -> Int -> a                  -- Retourne le i-ème élément d'une liste

-- Sous-listes
take :: Int -> [a] -> [a]                -- Prend le préfixe d'une liste de longueur donnée
drop :: Int -> [a] -> [a]                -- Prend le suffixe d'une liste en supprimant le nombre
                                         -- donné d'éléments au début

-- Transformations
map :: (a -> b) -> [a] -> [b]            -- Applique une fonction à chaque élément d'une liste
filter :: (a -> b) -> [a] -> [b]         -- Ne conserve que les éléments d'une liste vérifiant une condition
reverse :: [a] -> [a]                    -- Renverse une liste
intersperse :: a -> [a] -> [a]           -- Insère des copies d'un élément entre
                                         -- chacun des éléments d'une liste
intercalate :: [a] -> [[a]] -> [a]       -- Insère des copies d'une liste entre
                                         -- chacun des listes d'une liste
transpose :: [[a]] -> [[a]] -> [a]       -- Retourne la transposée d'une matrice
                                         -- (une matrice étant une liste de listes)
permutations :: [a] -> [[a]]             -- Retourne une liste de permutations
concat :: [[a]] -> [a]                   -- Aplatit une liste de listes en les concaténant
sort :: Ord a => [a] -> [a]              -- Trie une liste
nub :: Eq a => [a] -> [a]                -- Supprime les doublons d'une liste

-- Arithmétique
sum :: Num a => [a] -> a                 -- Retourne la somme des éléments d'une liste
product :: Num a => [a] -> a             -- Retourne le produit des éléments d'une liste
maximum :: Ord a => [a] -> a             -- Retourne le maximum d'une liste
minimum :: Ord a => [a] -> a             -- Retourne le minimum d'une liste

-- Prédicats
isPrefixOf :: Eq a => [a] -> [a] -> Bool -- Indique si une liste est préfixe d'une autre
isSuffixOf :: Eq a => [a] -> [a] -> Bool -- Indique si une liste est suffixe d'une autre
elem :: Eq a => a -> [a] -> Bool         -- Indique si un élément apparaît dans une liste
notElem :: Eq a => a -> [a] -> Bool      -- Indique si un élément n'apparaît pas dans une liste

-- Logique
and :: [a] -> a                          -- Retourne vrai si tous les éléments de la liste sont vrais
or :: [a] -> a                           -- Retourne vrai s'il existe un élément vrai dans la liste
all :: (a -> Bool) -> [a] -> a           -- Retourne vrai si tous les éléments de la liste
                                         -- vérifient une condition donnée
any :: (a -> Bool) -> [a] -> a           -- Retourne vrai s'il existe un élément de la liste
                                         -- vérifiant une condition donnée

-- Listes infinies
repeat :: a -> [a]                       -- Répète un élément à l'infini
cycle :: [a] -> [a]                      -- Répète les éléments d'une liste à l'infini
```

## 2 - Manipulation de listes

Récupérez le fichier [ExerciceListe.hs](ExerciceListe.hs) disponible dans le
dépôt et chargez-le dans l'interpréteur Haskell à l'aide de la commande:
```haskell
:l ExerciceListe.hs
```
Aussi, chargez les modules `Data.List` et `Data.Char` en tapant
```haskell
import Data.Char
import Data.List
```

En utilisant les fonctions suggérées entre parenthèses, répondez aux questions
suivantes. *Remarque*: même s'il est possible de le faire autrement qu'en
utilisant les fonctions suggérées, je vous encourage à respecter les
contraintes, afin de mieux apprendre à utiliser certaines fonctions. Si vous ne
vous rappelez plus de ce que fait la fonction, il peut toujours être utile
d'étudier sa signature avec la commande
```
:t nomFonction
```
Aussi, essayez d'obtenir la réponse en écrivant l'expression en **une seule
ligne**.

- (`sort`) Triez la liste `nombres` (disponible dans `ExerciceListe.hs`).
- (`nub`) Transformez la liste `nombres` de sorte qu'elle ne contienne aucun
  doublon.
- (`minimum`, `maximum`) Calculez l'étendue de la liste `nombres`. En
  statistiques, l'*étendue* d'une liste de nombre est la différence entre la
  valeur maximale et la valeur minimale.
- (`sum`, `realToFrac`, `genericLength`) Calculez la moyenne de `nombres`. Il
  peut être bon de vérifier d'abord pourquoi ça ne fonctionne pas en utilisant
  seulement `sum` et `length`.
- (`odd`, `filter`, `take`) Extraire les 10 premiers nombres de la liste
  `nombres` qui sont impairs.
- (`mod`, `div`) Produisez le couple `(q,r)` où `q` est le quotient et `r` le
  reste de la division de `147457` par `1297`.
- (`sum`) Montrez que la somme des entiers de 0 à 100 est égale à `100 * 101 / 2`.
- (`length`, `permutations`, `factorial`) Montrez que le nombre de permutations
  de la liste `[1..8]` est bien égal à `8!` (le point d'excalamation se lit
  "factoriel").
- (`transpose`) Montrez que la matrice `matrice` est symétrique (les valeurs
  sont invariantes quand on applique une réflexion par rapport à la diagonale).
- (`toLower`) Transformez `phrase` en lettre minuscule.
- (`toLower`, `filter`, `map`, `isLower`) Transformez `phrase` en minuscule et
  en supprimant les espaces et les signes de ponctuation.
- (`toLower`, `filter`, `map`, `isLower`, `reverse`) Montrez que `phrase` est
  une phrase palindromique, c'est-à-dire qu'on obtient la même phrase qu'on la
  lise de gauche à droite ou de droite à gauche, en supposant qu'on ignore la
  casse et les signes de ponctuation.
- (`filter`, `isSpace`, `length`) Compter le nombre d'espaces dans `phrase`.
- (`filter`, `isUpper`, `length`) Compter le nombre de lettres majuscules dans
  `phrase`.
- (`all`, `take`, `length`, `drop`, `isUpper`, `isAlpha`, `isDigit`) Montrez
  que `code` est bien un code permanent valide (4 lettres majuscules, suivies
  de 8 chiffres).

# 3 - Définition de fonctions

## 3.1 - La fonction pgcd

Implémentez la fonction qui calcule le plus grand commun diviseur de deux
entiers. Sa signature est
```haskell
pgcd :: Int -> Int -> Int
```
*Remarque*: Mathématiquement, *pgcd(0,0)* n'est pas défini, mais vous pouvez
supposer que *pgcd(0,0) = 0* pour simplifier.

## 3.2 - Périodes d'une liste

Soit `L` une liste de longueur `n` et `p` un entier tel que `0 < p < n`. On dit
que `p` est une *période* de `L` si `L[i] = L[i+p]` pour tout indice `i` valide
dans `L` tel que `L[i+p]` est aussi un indice valide.

Implémentez une fonction qui indique si un nombre est une période d'une liste,
dont la signature est
```haskell
estPeriode :: Eq a => Int -> [a] -> Bool
```
*Remarque*: Vous pouvez retourner systématiquement `False` si la période
indiquée n'est pas entre `1` et `n - 1`.

Par exemple, on s'attend au comportement suivant:
```haskell
Prelude> estPeriode 3 [1,2,3,1,2]
True
Prelude> estPeriode 0 [1,2,3,1,2,3]
False
Prelude> estPeriode 4 [1,2,3,1,1]
True
```

Ensuite, implémentez une fonction
```haskell
periodes :: Eq a => [a] -> [Int]
```
qui retourne la liste en ordre croissant de toutes les périodes d'une liste. On
s'attend donc au comportement suivant:
```haskell
Prelude> periodes [1,2,3,1,2]
[3]
Prelude> periodes "aaaaa"
[1,2,3,4]
Prelude> periodes [0,1,0,0,1,0,1,0,0,1,0]
[5,8]
```

## 3.3 - Supprimer les éléments d'une liste

Écrivez une fonction qui supprime tous les éléments d'une liste qui ne se
trouvent pas dans un intervalle donné (incluant les bornes), dont la signature
est
```haskell
supprimerIntervalle :: (Int,Int) -> [Int] -> [Int]
```

On s'attend au comportement suivant:
```haskell
Prelude> supprimerIntervalle (3,5) [1..7]
[3,4,5]
Prelude> supprimerIntervalle (6,8) [7,4,9,4,5,5,5,5,6,7,1,2,3,9]
[7,6,7]
```

## 3.4 - Affichage d'une grille de tic-tac-toe

Reprenez l'exemple de la grille de tic-tac-toe vu en classe et ajoutez une
fonction qui permet d'afficher une grille, dont la signature est
```haskell
showGrille :: Grille -> String
```

Rappelons les types utilisés:
```haskell
-- Une case de la grille peut être soit occupée par X, par O ou vide.
data Case = X | O | Vide
    deriving (Show, Eq)

-- Une grille de tic-tac-toe 3 x 3
type Grille = ((Case, Case, Case),
               (Case, Case, Case),
               (Case, Case, Case))
```

Nous aimerions avoir le comportement suivant:
```haskell
Prelude> showGrille ((X,O,Vide),(X,Vide,O),(X,Vide,Vide))
X | O |  
--+---+--
X |   | O
--+---+--
X |   |  
```

## 3.5 - Mots conjuguées

On dit que deux mots `s` et `t` sont *conjugués* s'il existe deux autres mots
`x` et `y` telles que `s = xy` et `t = yx`.  Par exemple, le mot `s = fromage`
et le mot `t = magefro` sont conjugués (on peut le vérifier avec `x = fro` et
`y = mage`).

Définissez une fonction dont la signature est
```haskell
sontConjugues :: [Char] -> [Char] -> Bool
```
qui retourne vrai si et seulement les deux mots passés en arguments sont
conjugués.

# 4 - Retour sur les listes

Réimplémentez les fonctions suivantes sur les listes, sans regarder le code
source:
```haskell
head :: [a] -> a
last :: [a] -> a
tail :: [a] -> [a]
init :: [a] -> [a]
null :: [a] -> Bool
length :: [a] -> Int
replicate :: Int -> a -> [a]
take :: Int -> [a] -> [a]
drop :: Int -> [a] -> [a]
map :: (a -> b) -> [a] -> [b]
filter :: (a -> b) -> [a] -> [b]
```
Notez que vous devez les renommer (car elles sont chargées par `Prelude`). La
stratégie conventionnelle consiste à ajouter le caractère prime (`'`) à la fin
pour qu'il n'y ait pas de conflit.
