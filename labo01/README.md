# Laboratoire 1: Installation, configuration et introduction

## 1 - Installation

(Note: cet exercice n'est pas abordé en laboratoire et devrait être complété à
la maison.) Si vous avez un ordinateur portable et que vous souhaitez
l'utiliser pour développer vos travaux pratiques, il est important d'installer
le plus rapidement possible un environnement complet et fonctionnel sur votre
machine.  Voici ce qui est recommandé dans le cadre de ce cours :

### 1.1 - Gestionnaire de paquets

Un gestionnaire de paquets est un logiciel qui facilite l'installation de
logiciels et de bibliothèques sur une machine. C'est très, très pratique!

- Utilisateurs *Linux*. La très grande majorité des plateformes Linux sont
  livrées avec un gestionnaire de paquets. Par exemple, sur les systèmes Debian
  (Mint/Ubuntu), il s'agit d'[Aptitude](https://doc.ubuntu-fr.org/aptitude)
  (`apt`). 

- Utilisateurs *Mac OS*. Assurez-vous d'avoir installé XCode. Les deux
  gestionnaires de paquets les plus populaire sont
  [MacPorts](https://www.macports.org/) et
  [Homebrew](https://brew.sh/index_fr.html).

- Utilisateurs *Windows*. Il est recommandé d'installer un système Linux sur
  votre machine personnelle, mais ce n'est pas obligatoire. Cela vous permettra
  d'installer plus facilement des outils complémentaires, comme Haddock et
  Cabal. Ceci dit, il est tout de même possible de réaliser le cours complet
  sur un système Windows.

  Si vous êtes tout de même intéressé à installer une plateforme Linux, je vous
  recommande les deux solutions suivantes:

  - En partition double (*dual boot*). Cette solution est préférable à la
    suivante si vous souhaitez avoir une meilleure expérience et moins de
    problèmes techniques à résoudre (délais, utilisation sous-optimale du
    matériel, etc.). S'il s'agit de votre première expérience Linux, je vous
    recommande d'utiliser [Ubuntu](https://www.ubuntu.com/) ou [Linux
    Mint](https://www.linuxmint.com/)
  - Installer une machine virtuelle Linux. Voir par exemple la capsule
    <https://www.youtube.com/watch?v=1zfO-Fhqyb8> du CLibre sur le sujet.

  Le CLibre est une organisation qui relève de la Faculté des sciences et donc
  le mandat est de promouvoir le logiciel libre. N'hésitez pas à les contacter
  si vous avez besoin d'aide pour cette étape (<http://clibre.uqam.ca/>).

### 1.2 - Installation de GHC

Le compilateur Haskell que vous utiliserez est
[GHC](https://www.haskell.org/ghc/). Si vous avez un gestionnaire de paquets,
il suffit d'entrer une commande. Par exemple, sous Ubuntu, il suffit de taper
```
sudo apt-get install ghc ghc-prof ghc-doc
```
Il est également possible de l'installer manuellement ou à partir du code
source, mais ce n'est pas du tout nécessaire dans le cadre du cours.

## 2 - Établir une connexion SSH

Dans le cadre de ce cours, des comptes ont été créés pour chacun d'entre vous
sur le serveur Java (`java.labunix.uqam.ca`). Pour vous y connecter, vous devez
connaître votre identifiant MS (2 lettres suivies de 6 chiffres) ainsi que
votre NIP.

Familiarisez-vous avec les étapes de base pour établir une connexion SSH. Le
mécanisme sera différent selon que vous travaillez sous Windows, Mac OS ou
Linux. Il s'agit d'une partie très importante, car vos travaux pratiques
devront minimalement fonctionner sur le serveur Java.

Parfois, certains comptes étudiants ne sont pas activés sur les serveurs, alors
adressez-vous aux techniciens du LaMISS ou envoyez-moi un courriel si c'est
votre cas et le problème devrait être réglé rapidement.

## 3 - Choix d'un éditeur de texte

Pour écrire vos programmes en Haskell et en Prolog, un simple éditeur de texte
est suffisant.  Si vous n'avez pas de préférence, je vous encourage à apprendre
Vim, qui est un des éditeurs de texte les plus utilisés. Pour s'y initier, il
suffit d'ouvrir un terminal et de taper la commande
```shell
vimtutor
```
puis de suivre les instructions. Personnellement, j'utiliserai Vim toute la
session.

**Note:** Éviter les éditeur de texte qui ne supportent pas directement le
format UTF8. Assurez-vous aussi que la coloration syntaxique soit activée.

## 4 - Markdown

Si vous ne connaissez pas déjà le format Markdown, assurez-vous de comprendre
son fonctionnement. Pour cela, il suffit de lire rapidement la page
https://daringfireball.net/projects/markdown/syntax

Lors de la remise de vos travaux pratiques, vous devrez toujours joindre un
fichier nommé `README.md` qui décrira votre projet. Vous pouvez par exemple
indiquer le titre du projet, l'auteur et une ou deux phrases qui décrivent ce
qu'il fait, comment le faire fonctionner, s'il y a des dépendances, etc.
Portez attention en particulier aux éléments suivants:

- Créer des titres et des sections;
- Segmenter le texte en paragraphes;
- Faire des liens vers une autre page;
- Montrer des bouts de code (courts ou blocs);
- Mettre du texte en italique ou en gras;
- Insérer une image;
- etc.

## 5 - Premiers pas en Haskell

Ouvrez un éditeur de texte et créez un nouveau fichier dont le contenu est le
même que celui du fichier `TriRapide.hs` [disponible dans ce
dépôt](TriRapide.hs). Sauvegardez-le en appelant le fichier `TriRapide.hs`.
(En Haskell, on recommande que les noms de fichier commencent par une
majuscule.)

Ensuite, lancez l'interpréteur de GHC. Il suffit d'entrer la commande
```
ghci
```
Vous devriez voir apparaître une invite de commande qui ressemble à ceci:
```
GHCi, version 7.8.3: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Prelude> 
```
Saisissez quelques instructions dans l'interpréteur pour vous habituer (par
exemple, des opérations arithmétiques, des opérations sur les listes, etc.).
Notez qu'il suffit d'appuyer sur CTRL-D (le caractère de fin de fichier) pour
quitter l'interpréteur.

Chargez le programme `TriRapide.hs` à l'aide de l'instruction
```
:l TriRapide.hs
```
**Remarque:** l'interpréteur offre la complétion automatique si vous appuyez
sur TAB pour compléter le nom du fichier à charger.

Lorsque le chargement est complété, vous pouvez sans problème utiliser la
fonction `qsort`, par exemple en entrant
```haskell
qsort [2,3,1,4]
```
ou encore
```haskell
qsort "abracadabra"
```
(Rappelons qu'une chaîne de caractères est un cas particulier de liste.)

Il est aussi possible de compiler le module GHC pour qu'il produise un
exécutable. Pour cela, il suffit d'entrer la commande
```
ghc -o tri TriRapide.hs
```
qui produit plusieurs fichiers (`TriRapide.o`, `TriRapide.hi`, `tri`).
Évidemment, vous pouvez remplacer `tri` par n'importe quel autre nom de votre
choix. Puis, vous pouvez exécuter le programme avec
```
./tri
```

Pendant la première partie de la session, vous allez principalement développer
vos programmes Haskell à l'aide de l'interpréteur. Plus précisément, vous allez
écrire des fonctions dans un fichier avec l'extension `.hs` que vous allez
charger dans l'interpréteur afin d'effectuer différents tests. La compilation
en un fichier exécutable sera plus rarement utilisée.

## 6 - Configuration de Git

Pour conclure cette séance, un bref survol de l'utilisation de Git et de la
plateforme GitLab est présenté.

- Rendez-vous tout d'abord sur le site de [GitLab](https://gitlab.com/) pour
  vous créer un compte. Prenez un nom d'utilisateur significatif (par exemple,
  le mien est `ablondin`), car il est probable que vous le réutiliserez plus
  tard dans un cadre autre que ce cours. Évitez les noms bizarres, comme
  `demoniacbrain` ou `lord-of-the-ring`, car il y a de bonnes chances pour que
  ce compte vous soit utile après le cours dans un contexte professionnel
  (évidemment, ce n'est qu'une suggestion !).

- Créez un nouveau dépôt, donnez-lui un nom significatif, laissez la
  description vide et les autres options par défaut puis confirmez la création.

- Maintenant, à l'aide de Vim, ouvrez le fichier `~/.gitconfig` et entrez les
  lignes suivantes

    ```ini
    [user]
        name = <votre nom>
        email = <votre courriel>
    [color]
        ui = auto
    [core]
        editor = vim
    ```

- Dans un terminal, rendez-vous dans le répertoire qui contient votre projet
  avec les fichiers `TriRapide.hs` et `README.md`. Initialisez un projet avec
  la commande

    ```shell
    git init
    ```

- Ensuite, indiquez à Git que vous souhaitez versionner les deux fichiers
  décrits plus haut

    ```shell
    git add TriRapide.hs README.md
    ```

- Si vous tapez `git status`, vous devriez voir que les deux fichiers sont
  ajoutés, mais que d'autres fichiers (comme `TriRapide.o`, `TriRapide.hi`,
  l'exécutable `tri`) n'ont pas été ajoutés.  **Ne les ajoutez pas**, car ils
  ne doivent pas être versionnés par Git.

- Toujours dans le répertoire courant, créez un fichier nommé `.gitignore` (le
  point initial est important) dans lequel vous insérez ce qui suit :

    ```shell
    *.o
    *.hi
    tri
    ```

- Ajoutez ce fichier également à l'aide de la commande `git add .gitignore`.

- Ensuite, entrez

    ```shell
    git commit
    ```

- Cela devrait ouvrir Vim pour que vous écriviez un message de commit. Écrivez
  quelque chose du genre

    ```shell
    Première version de mon premier projet Haskell!
    ```

- Ensuite, il vous faut établir le lien entre votre dépôt local et celui sur
  GitLab. Pour cela, il suffit d'entrer la commande

    ```shell
    git remote add origin git@gitlab.com:<nom d'utilisateur>/<nom du projet>.git
    ```

    (Au début, je ne me rappelais jamais de la commande par coeur, alors je me
    rendais sur le site de GitLab et je cliquais sur le nouveau dépôt créé, qui
    indique les commandes à entrer dans une boîte sur la page d'accueil vers le
    bas.)

- Finalement, entrez la commande

    ```shell
    git push origin master
    ```

- Ceci devrait avoir pour effet de "pousser" vos modifications locales sur le
  dépôt à distance. Vous pouvez le visualiser dans un fureteur. En particulier,
  votre fichier `README.md` devrait apparaître sous forme HTML dans le bas de
  la page d'accueil du projet. Assurez-vous toujours de bien respecter le
  format Markdown pour que la génération du fichier HTML apparaisse
  correctement en visualisant l'ensemble du fichier.
